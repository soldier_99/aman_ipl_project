// Matches won per team per Year

//can be done by iterating thru obj1 and matching year with jsonArr and then editing the obj.


let matchesWon = (jsonArr) =>{

    let matchesWonPerTeamPerYear = jsonArr.reduce((accumulator,match)=>{
        let team = match.winner;
        let year = match.season;
        if(!accumulator[team]){
            accumulator[team] = {};
            accumulator[team][year] = 1;
        }else{
            if(!accumulator[team][year]){
                accumulator[team][year] = 1;
            }else{
                accumulator[team][year]++;
            }
        }
        return accumulator;
    },{})

    return JSON.stringify(matchesWonPerTeamPerYear);

}


const csv = require('../../node_modules/csv-parser')
const fs = require('fs')
const final = [] ;

fs.createReadStream('../data/matches.csv')
.pipe(csv({})).on('data',(data)=> final.push(data))
.on('end',()=>{
    

   let j =  matchesWon(final);


   fs.writeFile('../public/output/matchesWon.json', j, err => {
       if (err) {
            console.error(err)
       return
      }
    })


})
