//extra runs per team in 2016

const csv = require('../../node_modules/csv-parser')
const fs = require('fs')
const matchData=[];
const deliveryData = [];


const extraRuns = (matches, deliveries) =>{
    const extRuns = matches.reduce((accumulator, match)=>{
        for(let delivery of deliveries){
            //console.log(delivery)
            if(match.id == delivery.match_id){
                let team = delivery.bowling_team;
                if(!accumulator[team]){
                    accumulator[team] = parseFloat(delivery.extra_runs);
                }else{
                    accumulator[team] += parseFloat(delivery.extra_runs);
                }
            }
        }
        return accumulator;
    },{})
    return (extRuns);
}

fs.createReadStream('../data/deliveries.csv')
.pipe(csv({})).on('data',(data)=> deliveryData.push(data))
 .on('end',()=>{
    fs.createReadStream('../data/matches.csv')
    .pipe(csv({})).on('data',(data)=> matchData.push(data))
     .on('end',()=>{

        // for 2016

        const reqdMatchData = matchData.filter(element =>{
            if(element.season == '2016'){
                return element;
            }
        })

        const extraa = extraRuns(reqdMatchData, deliveryData);
        const jsonExtra = JSON.stringify(extraa);
        //console.log(jsonExtra) ;

        fs.writeFile('../public/output/extraRuns.json', jsonExtra, err => {
            if (err) {
                 console.error(err)
            return
           }
         })
})
})

