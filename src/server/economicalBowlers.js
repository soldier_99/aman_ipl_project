// top 10 economical bowlers in 2015..

const csv = require('../../node_modules/csv-parser')
const fs = require('fs')
const matchData=[];
const deliveryData = [];

const economicalBowlers = (matches,deliveries) =>{
    const topBowlers = matches.reduce((accumulator, match)=>{
        for(let delivery of deliveries){
            if(match.id == delivery.match_id){
                let bowler = delivery.bowler;
                if(!accumulator[bowler]){
                    accumulator[bowler] = { runs: parseInt(delivery.total_runs), balls: 1};
                    
                }else{
                    accumulator[bowler].runs += parseInt(delivery.total_runs);
                    accumulator[bowler].balls++;
                }
        }
    }
    return accumulator;
    },{})

    const ecoBowlersArr = Object.entries(topBowlers);

    const topEconomicalBowlers = ecoBowlersArr.reduce((accumulator, currentBowler) =>{
        let bowler = currentBowler[0];
        let eco = parseFloat((currentBowler[1].runs/currentBowler[1].balls).toFixed(2));

        if(!accumulator[bowler]){
            accumulator[bowler] = eco;
        }
        return accumulator ;

    },{})

    const newArr = Object.entries(topEconomicalBowlers);

    newArr.sort((a,b) => {
        return a[1] - b[1];
    })

    const  finalObj = {};
    for(let counter in newArr){
        // console.log(counter);
        if (counter == 10){
            break;
        } 
        
        finalObj[newArr[counter][0]] = newArr[counter][1];
    }
     return finalObj;


    // let runsArr = Object.values(topEconomicalBowlers);
    // console.log(runsArr);
}

fs.createReadStream('../data/deliveries.csv')
.pipe(csv({})).on('data',(data)=> deliveryData.push(data))
 .on('end',()=>{
    fs.createReadStream('../data/matches.csv')
    .pipe(csv({})).on('data',(data)=> matchData.push(data))
     .on('end',()=>{

        // for 2015

        const reqdMatchData = matchData.filter(element =>{
            if(element.season == '2015'){
                return element;
            }
        })

        let top10EcoBowlers = economicalBowlers(reqdMatchData, deliveryData);
        top10EcoBowlers = JSON.stringify(top10EcoBowlers);
        console.log(top10EcoBowlers);

        fs.writeFile('../public/output/economicalBowlers.json', top10EcoBowlers, err => {
            if (err) {
                 console.error(err)
            return
           }
         })

})
})